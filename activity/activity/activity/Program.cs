﻿using System;
using System.Collections;

namespace Activity{
    class Activity
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to your shopping list app!");
            Console.WriteLine("Press 1 to Proceed");
            Console.WriteLine("Press 5 to Exit");


            int optionSelection = Convert.ToInt16(Console.ReadLine());
            ArrayList myItemList = new ArrayList();

            while (optionSelection < 5 && optionSelection > 0)
            {
                Console.WriteLine("============== MENU ==============");
                Console.WriteLine("Choose an option:");
                Console.WriteLine("[1] Display all items in shopping list");
                Console.WriteLine("[2] Add new item in shopping list");
                Console.WriteLine("[3] Remove an item in shopping list");
                Console.WriteLine("[4] Clear all items in shopping list");
                Console.WriteLine("[5] Exit");
                Console.WriteLine("==============      ==============");

                int menu = Convert.ToInt16(Console.ReadLine());
                switch (menu)
                {
                    case 1:
                        Console.WriteLine("------ Current Shopping List Item ------");
                        foreach (string arrayListItem in myItemList)
                        {
                            int index_arrayListItem = myItemList.IndexOf(arrayListItem);
                            Console.WriteLine("["+index_arrayListItem+"] " + arrayListItem );
                        }
                        Console.WriteLine();
                        break;

                    case 2:
                        Console.WriteLine("------ Insert New Item ------");
                        string newItem = Console.ReadLine();
                        myItemList.Add(newItem);
                        Console.WriteLine(newItem + " added succesfully");
                        break;

                    case 3:
                        Console.WriteLine("Enter the Index of the Item to Remove");
                        foreach (string arrayListItem in myItemList)
                        {
                            int index_arrayListItem = myItemList.IndexOf(arrayListItem);
                            Console.WriteLine("[" + index_arrayListItem + "] " + arrayListItem);
                        }
                        Console.WriteLine();

                        int removeItem = Convert.ToInt16(Console.ReadLine());
                        Console.WriteLine(myItemList[removeItem] + " has been removed");
                        myItemList.RemoveAt(removeItem);
                        break;
                    case 4:
                        myItemList.Clear();
                        Console.WriteLine("All items in shopping list cleared.");
                        break;

                    default:
                        Console.WriteLine("App is Closed");
                        optionSelection = 5;
                        break;
                }
            }


        }
    }
}