﻿// The "using" keyword is used to include namespaces in a program
// A namespace is a collection of classes
using System;
using System.Collections;

// A namespace is a collection of classes
//The "namespace" keyword is used to declare a namespace
namespace discussion { 
    class Discussion
    {
        //Serves as the entry point of the application
        static void Main(String[] args)
        {
            // Allows to printout values and messages in the console
            Console.WriteLine("Hello Batch 348!");

            // [Section] Variables, Data Types and Constants
            string myString = "Zuitt Asset Manager";

            // Value Types
            //When creating a variable in c# we add the data type, the name of the variable and we can initialize the value with an assignment operator (=)
            int myInt = 1; // max of 2.1 billion
            float myFloat = 5.7F; //32-bit
            double myDouble = 2.5D; //64-bit
            decimal myDecimal = 8.4M; //128-bit
            long myLong = 57390L; //max of 9.2 quintillion value
            bool myBoolean = false;
            const double interest = 5.3;

            Console.WriteLine("Result of Data Types");
            Console.WriteLine(myString);
            Console.WriteLine(myInt);
            Console.WriteLine(myFloat);
            Console.WriteLine(myDouble);
            Console.WriteLine(myDecimal);
            Console.WriteLine(myLong);
            Console.WriteLine(myBoolean);

            // Console.Writeline also supports the composite string format. Wherein we can integrate the values of the given items into index placeholders
            Console.WriteLine("double: {0}, decimal: {1}, boolean: {2}", myDouble, myDecimal, myBoolean);

            // [SECTION] Operators
            int numA = 10;
            int numB = 3;

            // Arithmetic Operators
            //Perform mathematical operation on 2 operands and return the appropriate value which we can also save in a variable
            Console.WriteLine();
            Console.WriteLine("Result of Arithmetic Operators");
            Console.WriteLine(numA + numB);
            Console.WriteLine(numA - numB);
            Console.WriteLine(numA * numB);
            Console.WriteLine(numA / numB);
            Console.WriteLine(numA % numB);

            //Increment
            numA++;
            Console.WriteLine(numA);

            //Decrement
            numA--;
            Console.WriteLine(numA);

            //Relational Operators
            Console.WriteLine();
            Console.WriteLine("Result of Relational Operators");
            Console.WriteLine(numA == numB);
            Console.WriteLine(numA != numB);
            Console.WriteLine(numA > numB);
            Console.WriteLine(numA < numB);
            Console.WriteLine(numA >= numB);
            Console.WriteLine(numA <= numB);

            //Logical Operators
            Console.WriteLine();
            Console.WriteLine("Result of Logical Operators");
            Console.WriteLine(numA == numB || numA > numB);
            Console.WriteLine(numA == numB && numA > numB);
            Console.WriteLine(!myBoolean);


            //Performs mathematical operation on our operands but saves/assigns the result in the left operand variable.
            //Arithmetic Assignment Operators
            Console.WriteLine();
            Console.WriteLine("Result of Arithmetic Operators");
            Console.WriteLine(numA += numB);
            Console.WriteLine(numA -= numB);
            Console.WriteLine(numA *= numB);
            Console.WriteLine(numA /= numB);
            Console.WriteLine(numA %= numB);
            Console.WriteLine();

            // [SECTION] Conditional Statements
            // if, else if, else statements
            Console.WriteLine("Result of Conditional Statements");
            if (numA > numB)
            {
                Console.WriteLine("numA is greater than numB");
            } else if (numA == numB)
            {
                Console.WriteLine("numA is equal to numB");
            } else
            {
                Console.WriteLine("numA is less than numB");
            }


            /*
             * 
            //Switch Statements
            Console.WriteLine();
            Console.WriteLine("What day of the week is today:");

            // The Console.ReadLine() method is used to read a line of text input from the console, and it returns the input as a string.
            // The "ToLower" method returns a copy of the string converted to all lowercase letters
           string day = Console.ReadLine().ToLower();
            switch (day)
            {
                case "monday":
                    Console.WriteLine("Red");
                    break;
                case "tuesday":
                    Console.WriteLine("Blue");
                    break;
                case "wednesday":
                    Console.WriteLine("Green");
                    break;
                default:
                    Console.WriteLine("Black");
                    break;
            }

            */

            //Ternary Operator
            Console.WriteLine();
            Console.WriteLine("Result of Ternary Conditional Statement");
            Console.WriteLine(numA > numB ? "yes" : "no");

            // [SECTION] Loops
            //Allows us to repeat code blocks based on a condition

            //While Loop
            Console.WriteLine();
            Console.WriteLine("Result of While Loop");
            int numC = 5;
            while (numC > 0)
            {
                Console.WriteLine("Value of a: {0}", numC);
                numC--;
            }


            //Do While Loop
            Console.WriteLine();
            Console.WriteLine("Result of Do While Loop");
            int numD = 0;
            do
            {
                Console.WriteLine("Value of a: {0}", numD);
                numD++;
            } while (numD < 3);


            //For Loop
            Console.WriteLine();
            Console.WriteLine("Result of For Loop");
            for (int x = 0; x < 5; x++)
            {
                Console.WriteLine("Value of x: {0}", x);
            }


            // Continue Statements - will allow us to forcibly continue to the next loop
            Console.WriteLine();
            Console.WriteLine("Result of Continue Statement");
            int numE = 0;
            while (numE < 20)
            {
                if(numE % 5 == 0)
                {
                    Console.WriteLine("Value of numE: {0}", numE);
                }
                numE++;
                continue;
            }


            // Break Statements - forcibly stops the loop
            Console.WriteLine();
            Console.WriteLine("Result of Break Statement");
            int numF = 0;
            while (numF < 20)
            {
                if (numF == 10)
                {
                    break;
                }
                if (numF % 5 == 0)
                {
                    Console.WriteLine("Value of numF: {0}", numF);
                }
                numF++;
            }



            // [Section] Arrays
            //It is used to store elements.
            //In C#, arrays are fixed-sized collections of elements of the same type.
            //To create an array, we also use the new keyword to create a new instance of an array.

            // Declaring Arrays
            //When creating a new array we also declare the type of array and the number of items in the array.
            // An array is a reference type, so the "new" keyword is used to create an instance of an array
            int[] sales = new int[3];

            // Assigning values to an array
            // C# arrays start with the 0 index value as the first element
            sales[2] = 27;
            sales[0] = 10;
            sales[1] = 5;

            Console.WriteLine();
            Console.WriteLine("Result from arrays:");
            Console.WriteLine(sales[2]);
            Console.WriteLine(sales[0]);
            Console.WriteLine(sales[1]);

            Console.WriteLine();
            Console.WriteLine($"First Element: {sales[0]} Second Element: {sales[1]} Third Element: {sales[2]}");

            Console.WriteLine(sales.Length);


            // Array declaration and initialization
            Console.WriteLine();
            string[] managers = new string[3]
            {
                "john", "jane", "joe"
            };

            //Using for loop
            Console.WriteLine("Result from array for loop:");
            for (int x = 0; x < managers.Length; x++)
            {
                Console.WriteLine(managers[x]);
            }

            //Using the For Each Loop
            Console.WriteLine();
            Console.WriteLine("Result from array for each loop:");
            foreach (string manager in managers)
            {
                Console.WriteLine(manager);
            }


            // [Section] Collections
            // ArrayList
            // A resizeable list represents an ordered collection of an object that can be indexed individually.

            // Declaring a list
            Console.WriteLine();
            ArrayList myArrayList = new ArrayList();

            // Adding elements to an array list
            myArrayList.Add("Harry");
            myArrayList.Add("Ron");
            myArrayList.Add("Hermione");

            Console.WriteLine("Result from adding Array list elements");
            foreach (string arrayListItem in myArrayList)
            {
                Console.WriteLine(arrayListItem);
            }



            // Declaring and initializing an array list
            Console.WriteLine();
            ArrayList customers = new ArrayList(new string[] {"Donald", "Mickey", "Goofy"});

            // Getting the index of an element
            int index_mickey = customers.IndexOf("Mickey");
            Console.WriteLine("Result from indexOf property");
            Console.WriteLine(index_mickey);

            // Getting the length of an Array List
            Console.WriteLine("Result from Count property");
            Console.WriteLine(customers.Count);

            // Getting elements from arraylist
            Console.WriteLine("Result from getting elements from Array listt");
            foreach (string customer in customers)
            {
                Console.WriteLine(customer);
            }



            // Adding elements in between an array list
            myArrayList.Insert(1, "Tom");
            Console.WriteLine("Result from adding Array List elements in between");
            foreach (string item in myArrayList)
            {
                Console.WriteLine(item);
            }

            // Removing elements from an array list
            // Removing single elements
            customers.Remove("Mickey");
            customers.RemoveAt(0);

            Console.WriteLine("Result from removing single Array List elements");
            foreach (string customer in customers)
            {
                Console.WriteLine(customer);
            }

            // Removing multiple elements
            // Removes 2 elements from index 1
            myArrayList.RemoveRange(1, 2);
            Console.WriteLine("Result from removing multiple Array List elements");
            foreach (string item in myArrayList)
            {
                Console.WriteLine(item);
            }

            // Updating array list elements
            myArrayList[0] = "Luna";
            myArrayList[1] = "Draco";
            Console.WriteLine("Result from updating Array List elements");
            foreach (string item in myArrayList)
            {
                Console.WriteLine(item);
            }

            // Sorting Arrays
            myArrayList.Sort();
            Console.WriteLine("Result from sorting Array List elements");
            foreach (string item in myArrayList)
            {
                Console.WriteLine(item);
            }

            // [Section] Hashtables
            // Hashtables represents a collection of key-value pairs.
            // It is used when you need to access elements by using keys.

            // Declaring and initialization hashtables
            Hashtable myHashtable = new Hashtable();

            Hashtable address = new Hashtable();

            address.Add("houseNumber", "221");
            address.Add("street", "Baker St.");
            address.Add("city", "London");
            address.Add("country", "England");

            // Getting the keys of a hashtable
            //To get the keys of a hashtable we can use the .Keys property.
            //However, to save it in a variable, we have to indicate the variable as ICollection type. This is the class of the keys returned by the hashtable

            ICollection addressKeys = address.Keys;

            Console.WriteLine("Result from accessing Hashtable keys");
            Console.WriteLine(address["street"]);

            Console.WriteLine("Result from Hashtable keys");
            foreach (string key in addressKeys)
            {
                Console.WriteLine(key);
            }






        }
    }
}